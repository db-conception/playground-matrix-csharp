namespace MatrixPlayground.Test;

using System.Drawing;
using MatrixPlayground.Five;

[TestClass]
public class PointageRequestTest
{
    [TestMethod]
    public void XPercentage_TryToSetAbove100Set100()
    {
        // Arrange
        int tryToSet = 150;
        int expected = 100;
        var req = new PointageRequest();

        // Act
        req.XPercentage = tryToSet;

        // Assert
        Assert.AreEqual(expected, req.XPercentage);
    }
    public void XPercentage_TryToSetBelow0Set0()
    {
        // Arrange
        int tryToSet = -45;
        int expected = 0;
        var req = new PointageRequest();

        // Act
        req.XPercentage = tryToSet;

        // Assert
        Assert.AreEqual(expected, req.XPercentage);
    }
    
    [TestMethod]
    public void XPercentage_TryToSetBetween0And100SetValue()
    {
        // Arrange
        int tryToSet = 60;
        int expected = 60;
        var req = new PointageRequest();

        // Act
        req.XPercentage = tryToSet;

        // Assert
        Assert.AreEqual(expected, req.XPercentage);
    }
    
    [TestMethod]
    public void YPercentage_TryToSetAbove100Set100()
    {
        // Arrange
        int tryToSet = 150;
        int expected = 100;
        var req = new PointageRequest();

        // Act
        req.YPercentage = tryToSet;

        // Assert
        Assert.AreEqual(expected, req.YPercentage);
    }
    public void YPercentage_TryToSetBelow0Set0()
    {
        // Arrange
        int tryToSet = -45;
        int expected = 0;
        var req = new PointageRequest();

        // Act
        req.YPercentage = tryToSet;

        // Assert
        Assert.AreEqual(expected, req.YPercentage);
    }
    
    [TestMethod]
    public void YPercentage_TryToSetBetween0And100SetValue()
    {
        // Arrange
        int tryToSet = 60;
        int expected = 60;
        var req = new PointageRequest();

        // Act
        req.YPercentage = tryToSet;

        // Assert
        Assert.AreEqual(expected, req.YPercentage);
    }
    
    
}