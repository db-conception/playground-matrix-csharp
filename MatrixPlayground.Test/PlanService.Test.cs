namespace MatrixPlayground.Test;

using System.Drawing;
using System.Numerics;
using MatrixPlayground.Five;

[TestClass]
public class PlanServiceTest
{
    [TestMethod]
    public void ConvertPointageRequestToPointForInFrontPlan_Success_ComputePointMiddleOfPlan()
    {
        // Arrange
        PointageRequest pr = new PointageRequest() { XPercentage = 50, YPercentage = 50};
        InFrontPlan ifp = new InFrontPlan(
            new Vector3(0, 0, 0),
            new Vector3(10,0, 0),
            new Vector3(10, 10, 0)
        );
        var planService = new PlanService();
        var expectedPoint = new Point(5, 5);

        // Act
        var computedPoint = planService.ConvertPointageRequestToPointForInFrontPlan(pr, ifp);

        // Assert
        Assert.AreEqual(computedPoint.X, expectedPoint.X);
        Assert.AreEqual(computedPoint.Y, expectedPoint.Y);
    }
    
    [TestMethod]
    public void ConvertPointageRequestToPointForInFrontPlan_Success_ComputePointTopRightCorner()
    {
        // Arrange
        PointageRequest pr = new PointageRequest() { XPercentage = 100, YPercentage = 100};
        InFrontPlan ifp = new InFrontPlan(
            new Vector3(0, 0,0),
            new Vector3(10,0,0),
            new Vector3(10, 10, 0)
        );
        var planService = new PlanService();
        var expectedPoint = new Point(10, 10);

        // Act
        var computedPoint = planService.ConvertPointageRequestToPointForInFrontPlan(pr, ifp);

        // Assert
        Assert.AreEqual(computedPoint.X, expectedPoint.X);
        Assert.AreEqual(computedPoint.Y, expectedPoint.Y);
    }

    [TestMethod]
    public void ApplyMatrix3DToInFrontPlan_NoChangementMatrice_ReturnSameThatInFront(){
        // Arrange
        var matrix = new LinearTransformation();
        matrix.Values[0,0] = 1;   matrix.Values[1,0] = 0;   matrix.Values[2,0] = 0;
        matrix.Values[0,1] = 0;   matrix.Values[1,1] = 1;   matrix.Values[2,1] = 0;
        matrix.Values[0,2] = 0;   matrix.Values[1,2] = 0;   matrix.Values[2,2] = 1;
        var planInFront = new InFrontPlan(
            new Vector3(1, 1, 0),
            new Vector3(10, 1, 0),
            new Vector3(10, 10, 0)
        );
        var service = new PlanService();
        
        // Act
        var angledPlan = service.ApplyMatrix3DToInFrontPlan(planInFront, matrix);

        // Assert
        Assert.AreEqual(planInFront.Corner1.X, angledPlan.Corner1.X);
        Assert.AreEqual(planInFront.Corner1.Y, angledPlan.Corner1.Y);
        Assert.AreEqual(planInFront.Corner2.X, angledPlan.Corner2.X);
        Assert.AreEqual(planInFront.Corner2.Y, angledPlan.Corner2.Y);
        Assert.AreEqual(planInFront.Corner3.X, angledPlan.Corner3.X);
        Assert.AreEqual(planInFront.Corner3.Y, angledPlan.Corner3.Y);
    }

    [TestMethod]
    public void ApplyLinearTransformationToVector3_TransformVectorSuccess(){
        // Arrange
        var vector = new Vector3(6, 8, 10);
        var transformation = new LinearTransformation() {};
        transformation.Values[0,0] = 0;   transformation.Values[1,0] = 0;   transformation.Values[2,0] = 4;
        transformation.Values[0,1] = 4;   transformation.Values[1,1] = 0;   transformation.Values[2,1] = 0;
        transformation.Values[0,2] = 0;   transformation.Values[1,2] = 4;   transformation.Values[2,2] = 0;
        var expectedVector = new Vector3(40, 24, 32);
        var planService = new PlanService();

        // Act
        var transformedVector = planService.ApplyLinearTransformationToVector3(vector, transformation);

        // Assert
        Assert.AreEqual(expectedVector.X, transformedVector.X);
        Assert.AreEqual(expectedVector.Y, transformedVector.Y);
        Assert.AreEqual(expectedVector.Z, transformedVector.Z);
    }


}