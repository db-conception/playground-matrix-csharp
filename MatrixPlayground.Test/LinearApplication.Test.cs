namespace MatrixPlayground.Test;

using System.Drawing;
using MatrixPlayground.Five;

[TestClass]
public class LinearApplicationTest
{
    [TestMethod]
    public void ICol_GetGoodResult_FromInitalMatrix()
    {
        // Arrange
        var matrix = new LinearTransformation();
        var expected = new double[3] { 1, 0, 0};

        // Act
        var col = matrix.ICol;

        // Assert
        Assert.AreEqual(expected[0], col[0]);
        Assert.AreEqual(expected[1], col[1]);
        Assert.AreEqual(expected[2], col[2]);
    }
    
    [TestMethod]
    public void JCol_GetGoodResult_FromInitalMatrix()
    {
        // Arrange
        var matrix = new LinearTransformation();
        var expected = new double[3] { 0, 1, 0};

        // Act
        var col = matrix.JCol;

        // Assert
        Assert.AreEqual(expected[0], col[0]);
        Assert.AreEqual(expected[1], col[1]);
        Assert.AreEqual(expected[2], col[2]);
    }
    
    [TestMethod]
    public void KCol_GetGoodResult_FromInitalMatrix()
    {
        // Arrange
        var matrix = new LinearTransformation();
        var expected = new double[3] { 0, 0, 1};

        // Act
        var col = matrix.KCol;

        // Assert
        Assert.AreEqual(expected[0], col[0]);
        Assert.AreEqual(expected[1], col[1]);
        Assert.AreEqual(expected[2], col[2]);
    }

}