namespace MatrixPlayground.Five;

using System.Numerics;

/**
 * Represente a Cartesian plan in front
 */
public class InFrontPlan : Plan{
    public InFrontPlan(Vector3 C1, Vector3 C2, Vector3 C3){
        this.Corner1 = C1;
        this.Corner2 = C2;
        this.Corner3 = C3;
    }
    protected override string NameOfPlan => "Plan In Front";
}