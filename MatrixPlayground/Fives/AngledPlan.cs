namespace MatrixPlayground.Five;

using System.Numerics;

/**
 * Represente a Cartesian plan not in front, angled in 3D.
 */
public class AngledPlan : Plan{

    protected override string NameOfPlan => "Angled Plan";

    public AngledPlan(){
    }
    public AngledPlan(Vector3 C1, Vector3 C2, Vector3 C3){
        this.Corner1 = C1;
        this.Corner2 = C2;
        this.Corner3 = C3;
    }
}