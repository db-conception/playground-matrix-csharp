namespace MatrixPlayground.Five;

public class LinearTransformation{

    public LinearTransformation()
    {
        this.Values[0,0] = 1;   this.Values[1,0] = 0;   this.Values[2,0] = 0;
        this.Values[0,1] = 0;   this.Values[1,1] = 1;   this.Values[2,1] = 0;
        this.Values[0,2] = 0;   this.Values[1,0] = 0;   this.Values[2,2] = 1;
    }

    public double[,] Values { get; set; } = new double[3,3];
    public double[] ICol
    {
        get { 
            return new double[3] {this.Values[0,0], this.Values[0,1], this.Values[0,2]};
        }
    }
    
    public double[] JCol
    {
        get { 
            return new double[3] {this.Values[1,0], this.Values[1,1], this.Values[1,2]};
        }
    }

    public double[] KCol
    {
        get { 
            return new double[3] {this.Values[2,0], this.Values[2,1], this.Values[2,2]};
        }
    }

    public void DrawMatrice(){
        System.Console.WriteLine(
            $"|{this.Values[0,0]} {this.Values[1,0]} {this.Values[2,0]}|\n" +
            $"|{this.Values[0,1]} {this.Values[1,1]} {this.Values[2,1]}|\n" +
            $"|{this.Values[0,2]} {this.Values[1,2]} {this.Values[2,2]}|\n"
        );
    }
    
}