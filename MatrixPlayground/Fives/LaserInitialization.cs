namespace MatrixPlayground.Five;

using System.Drawing;

public class LaserInitialization{
    public LaserInitialization(AngledPlan angledPlan){
        AngledPlan = angledPlan;
    }

    public AngledPlan AngledPlan { get; }
}