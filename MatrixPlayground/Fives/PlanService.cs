namespace MatrixPlayground.Five;

using System.Drawing;
using System.Numerics;

public class PlanService{

    public PlanService(){
        this.PerfectInFrontPlan = new InFrontPlan(
            new Vector3(0, 0, 0), // C1 in a perfect plan
            new Vector3(8, 0, 0), // C1 in a perfect plan
            new Vector3(8, 14, 0) // C2 in a perfect plan
        );
    }

    public InFrontPlan PerfectInFrontPlan { get; private set; }

    public InFrontPlan TransformToInFrontPlan(AngledPlan angledPlan){

        // Represent the LinearTransformation between a perfect view in front of the plan and the current situation
        // var currentLinearTransformation = new Matrix3x2()
        throw new NotImplementedException();
    }

    public Vector3 ConvertPointageRequestToPointForInFrontPlan(PointageRequest pointageRequest, InFrontPlan perfectInFrontPlan){
        var boxHeight = perfectInFrontPlan.Corner1.Y + perfectInFrontPlan.Corner3.Y;
        var offsetYInsideBox = (float)boxHeight * ((float)pointageRequest.YPercentage / (float)100.0);
        int offsetYInsideBoxInteger = Convert.ToInt32(offsetYInsideBox);
        // System.Console.WriteLine($"height : {boxHeight}\n offset  : {offsetYInsideBoxInteger}");
        var yOfPoint = perfectInFrontPlan.Corner1.Y + offsetYInsideBoxInteger;
        
        var boxWidth = perfectInFrontPlan.Corner1.X + perfectInFrontPlan.Corner3.X;
        float offsetXInsideBox = (float)boxWidth * ((float)pointageRequest.XPercentage / (float)100.0);
        int offsetXInsideBoxInteger = Convert.ToInt32(offsetXInsideBox);
        // System.Console.WriteLine($"width : {boxWidth}\n offset  : {offsetXInsideBoxInteger}");
        var xOfPoint = perfectInFrontPlan.Corner1.X + offsetXInsideBoxInteger;
        
        return new Vector3(xOfPoint, yOfPoint, 0);
    }

    public LinearTransformation GenerateARandomMatrix()
    {
        // return new Matrix3D();

        var scaleByTwoMatrix = new LinearTransformation();
        scaleByTwoMatrix.Values[0,0] = 2;   scaleByTwoMatrix.Values[1,0] = 0;   scaleByTwoMatrix.Values[2,0] = 0;
        scaleByTwoMatrix.Values[0,1] = 0;   scaleByTwoMatrix.Values[1,1] = 2;   scaleByTwoMatrix.Values[2,1] = 0;
        scaleByTwoMatrix.Values[0,2] = 0;   scaleByTwoMatrix.Values[1,2] = 0;   scaleByTwoMatrix.Values[2,2] = 2;
        return scaleByTwoMatrix;
    }

    public AngledPlan ApplyMatrix3DToInFrontPlan(InFrontPlan inFrontPlan, LinearTransformation matrix3D){
        
        var angledPlan = new AngledPlan();
        foreach (var anyPoint in inFrontPlan.Vector3s)
        {
            var transformedVector = this.ApplyLinearTransformationToVector3(anyPoint, matrix3D);
            if(anyPoint == inFrontPlan.Corner1){
                angledPlan.Corner1 = transformedVector;
            }
            else if(anyPoint == inFrontPlan.Corner2){
                angledPlan.Corner2 = transformedVector;
            }
            else if(anyPoint == inFrontPlan.Corner3){
                angledPlan.Corner3 = transformedVector;
            }
            else{
                angledPlan.OthersVectors.Add(transformedVector);
            }
            
        }

        return angledPlan;
    }

    public Vector3 ApplyLinearTransformationToVector3(Vector3 vector, LinearTransformation transformation){
        double newX, newY, newZ;
        double x = vector.X;
        double y = vector.Y;
        double z = vector.Z;
        double A1 = transformation.Values[0,0], A2 = transformation.Values[1,0], A3 = transformation.Values[2, 0];
        double B1 = transformation.Values[0,1], B2 = transformation.Values[1,1], B3 = transformation.Values[2, 1];
        double C1 = transformation.Values[0,2], C2 = transformation.Values[1,2], C3 = transformation.Values[2, 2];

        newX = (x*A1) + (y*A2) + (z*A3);
        newY = (x*B1) + (y*B2) + (z*B3);
        newZ = (x*C1) + (y*C2) + (z*C3);

        Console.WriteLine($"\n|{newX}|\n|{newY}|\n|{newZ}|");

        return new Vector3(float.Parse(newX.ToString()), float.Parse(newY.ToString()), float.Parse(newZ.ToString()));
    }
}