namespace MatrixPlayground.Five;

using System.Numerics;

/**
 * Represente a Cartesian Plan
 */
public abstract class Plan{
    public Vector3 Corner1 { get; set; }

    public Vector3 Corner2 { get; set; }
    
    public Vector3 Corner3 { get; set; }

    public List<Vector3> OthersVectors { get; set; } = new List<Vector3>();

    public Vector3[] Vector3s 
    { 
        get {
            var listOfTotalVector3 = new List<Vector3>();
            listOfTotalVector3.Add(this.Corner1);
            listOfTotalVector3.Add(this.Corner2);
            listOfTotalVector3.Add(this.Corner3);
            listOfTotalVector3.AddRange(this.OthersVectors);
            return listOfTotalVector3.ToArray();
        } 
    }

    public OriginType OriginType { get; set; } = OriginType.BottomLeft;

    protected virtual string NameOfPlan => "Plan Abstract";


    public void DrawPlan(){
        int maxI = 20, maxJ = 20;

        string coinChar = "C";
        string emptyChar = "°";
        string otherVector3Char = "X";

        string output = "";
        
        var coins = new List<Vector3>(3) {this.Corner1, this.Corner2, this.Corner3};

        for (int iteratorJ = 0; iteratorJ < maxJ; iteratorJ++)
        {
            string line = "";
            for (int iteratorI = 0; iteratorI < maxI; iteratorI++)
            {
                Vector3 hittenCorner = coins.FirstOrDefault(p => p.X == iteratorI && p.Y == iteratorJ, new Vector3(-1, -1, 0));
                if(hittenCorner.X != -1 && hittenCorner.Y != -1){
                    line += coinChar;
                    continue;
                }
                Vector3 hittenAnotherVector3 = this.OthersVectors.FirstOrDefault(p => p.X == iteratorI && p.Y == iteratorJ, new Vector3(-1, -1, 0));
                if(hittenAnotherVector3.X != -1 && hittenAnotherVector3.Y != -1){
                    line += otherVector3Char;
                    continue;
                }
                line += emptyChar;
            }
            if(this.OriginType == OriginType.BottomLeft){
                output = line + "\n" + output; // We stack the new line above the previus
            }
        }

        System.Console.WriteLine(this.NameOfPlan + "\n\n" + output);
    }
}