namespace MatrixPlayground.Five;

/**
 * Indicate where fast want to point with the laser
 */
public class PointageRequest{
    
    private int xPercentage;
    public int XPercentage
    {
        get { return xPercentage; }
        set { 
            if(value < 101 && value > -1){
                xPercentage = value;
            }
            else if(value > 100) {
                xPercentage = 100;
            }
            else{
                xPercentage = 0;
            }
        }
    }
    

    private int yPercentage;
    public int YPercentage
    {
        get { return yPercentage; }
        set { 
            if(value < 101 && value > -1){
                yPercentage = value;
            }
            else if(value > 100) {
                yPercentage = 100;
            }
            else{
                yPercentage = 0;
            }
        }
    }
    
}