using System.Drawing;
using System.Numerics;
using System.Text.Json.Serialization;
using MatrixPlayground.Five;

public class PlaygroundLaser : IPlayground
{
    public void Play()
    {
        System.Console.WriteLine("Playground Laser");

        // JUST FOR DEMO
        System.Console.WriteLine("A perfect plan looklike : ");
        this.PlanService.PerfectInFrontPlan.DrawPlan();

        var savedInit = GetSavedInit();
        var angledPlan = savedInit.AngledPlan;
        angledPlan.DrawPlan();

        // FAKE Get request from Fast
        var fakePlayload = GetFakePlayload();

        // Convert percentage to vector
        var vector = PlanService.ConvertPointageRequestToPointForInFrontPlan(fakePlayload, this.PlanService.PerfectInFrontPlan);

        // JUST FOR DEMO
        // Draw the vector in a perfect plan
        System.Console.WriteLine("The vector requested in a perfect plan : ");
        this.PlanService.PerfectInFrontPlan.OthersVectors.Add(vector);
        this.PlanService.PerfectInFrontPlan.DrawPlan();
        
        // Determine the actual linear transformation (between InFrontPlan and actual AngledPlan)
        

        // Convert vector in perfect plan to vector in applyed angled plan
        var matrix = this.PlanService.GenerateARandomMatrix();
        matrix.DrawMatrice();

        // move the laser pointer
    }

    public PlanService PlanService { get; set; } = new PlanService();

    public LaserInitialization GetSavedInit(){
        return new LaserInitialization(new AngledPlan(
            new Vector3(4, 1, 0), // C1
            new Vector3(13, 3, 0), // C2
            new Vector3(10, 9, 0) // C2
        ));
    }

    public PointageRequest GetFakePlayload(){
        // Imagine a back of 2x2 like
        /*
        |A1|A2|
        |B1|B2|
        */
        // With an origin in the bottom left 
        // This will point on B1 
        return new PointageRequest() {
            XPercentage = 25,
            YPercentage = 25
        };
    }
}